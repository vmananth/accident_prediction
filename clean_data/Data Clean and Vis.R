data <- read.csv('Vehicle_Information.csv')
unique(data$Age_Band_of_Driver)
accidents <- read.csv('Accident_Information.csv')
unique(accidents$Day_of_Week)

colnames(accidents)

new_columns <- c('Accident_Index', 'Accident_Severity', 'Date', 
             'Day_of_Week', 'Did_Police_Officer_Attend_Scene_of_Accident',
             'Latitude', 'Longitude', 'Light_Conditions', 'Number_of_Casualties',
             'Number_of_Vehicles', 'Road_Surface_Conditions', 'Road_Type',
             'Speed_limit', 'Urban_or_Rural_Area', 'Weather_Conditions',
             'Year')

new_data <- accidents[, new_columns]

accidents_by_year <- data.frame(table(new_data$Year))
accidents_by_year$Var1 = as.numeric(as.character(accidents_by_year$Var1))

str(accidents_by_year)

unique(accidents_by_year$Var1)
xyplot(Freq ~ Var1, type=c("smooth", "p"))

library(ggplot2)
accidents_plot <- ggplot(accidents_by_year, aes(Var1,Freq)) + geom_point() +
scale_x_continuous(breaks = round(seq(min(accidents_by_year$Var1), max(accidents_by_year$Var1), by = 1),1)) +
geom_smooth()

accidents_plot +labs(title="Plot of accidents per year",
        x ="Year", y = "Accidents")
write.csv(new_data, file = "MyData.csv",row.names=FALSE)

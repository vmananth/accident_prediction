# Accident Prediction ARIMA model

### Plot Seasonality and Trend VIsualization

![alt text](https://github.com/ananthrgv/accident_prediction/blob/master/results/seasonality.png)

### Plot Forecast

![alt text](https://github.com/ananthrgv/accident_prediction/blob/master/results/forecast.png)